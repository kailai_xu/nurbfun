nrb = nrb4surf([0,0],[2,0],[0,2],[2,2]);
nrb = nrbdegelev(nrb,[3 3]);
a = linspace(0,1,20);
nrb = nrbkntins(nrb, {a(2:end-1), a(2:end-1)});
nrb = nrbfun(nrb);

%%
uexact = @(x,y) sin(pi*x) .* sin(pi*y);
Iu = nrbinterp(nrb, uexact);
Pu = nrbproject(nrb, uexact);

%%
figure('position',[500,500,800,400])
subplot(121)
plot(Iu)
subplot(122)
plot(Pu)

fprintf('l2 error between Iu and Pu : %g\n', norm(Iu-Pu,'l2'));
fprintf('L2 error\nInterpolate = %g, Projection = %g\n',errornorm(Iu, uexact, 'L2'),...
    errornorm(Pu, uexact, 'L2'));
