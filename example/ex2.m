% this script is a demonstration of higher order derivative computation
mresolution = 80;

tic
nrb = rectnrb(mresolution);
uexact = @(x,y) sin(pi*x).*sin(pi*y);
nrb = nrb.nrbinterp(uexact);

toc 

%%
x1 = rand;
x2 = rand;
u = nrbinverse(nrb, [x1,x2,0]);
nrb.eval(u(1),u(2)) - uexact(x1,x2)

y1 = nrb.eval1(u(1),u(2)); 
y2 = [pi * cos(pi*x1) * sin(pi*x2)
pi * sin(pi*x1) * cos(pi*x2)];
y1-y2


y1 = nrb.eval2(u(1),u(2)); 
y2 = [-pi^2 * sin(pi*x1) * sin(pi*x2)
    pi^2 * cos(pi*x1) * cos(pi*x2)
-pi^2 * sin(pi*x1) * sin(pi*x2)];
y1-y2

y1 = nrb.eval3(u(1),u(2)); 
y2 = [-pi^3 * cos(pi*x1) * sin(pi*x2)
    -pi^3 * sin(pi*x1) * cos(pi*x2)
    -pi^3 * cos(pi*x1) * sin(pi*x2)
-pi^3 * sin(pi*x1) * cos(pi*x2)];
y1-y2

