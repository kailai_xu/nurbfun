% This script demonstrate solving the Poisson problem 

clear all
close all
%% Make mesh
nrb = rectnrb(100);

%% PDE data
uexact = @(x,y) sin(pi*x) .* sin(pi*y);
f = @(x,y) 2*pi^2 * uexact(x,y);

%% Basic data 
noCtrlPts = prod(nrb.number);

uv = aveknt(nrb); % collocation points
u = uv{1}; v = uv{2};
[u,v] = meshgrid(u,v);
u = u(:);
v = v(:);

%% Assemble matrices
A = zeros(noCtrlPts);
rhs = zeros(noCtrlPts,1);

for i = 1:noCtrlPts
    N = nrb.index(u(i),v(i));
    [R, dR,ddR, coord, J] = nrb.phys2(u(i), v(i), N);
    A(i, N) =  - (ddR(1,:) + ddR(3,:)); % -\Delta 
    rhs(i) = f(coord(1),coord(2));        
end

%% apply boundary condition
bd = true(nrb.number);
bd(:,1) = false;
bd(:,end) = false;
bd(1,:) = false;
bd(end,:) = false; 
% this is the standard way to mark boundary condition

%% Solve
coeffs = zeros(noCtrlPts,1);
coeffs(bd) = A( bd, bd) \ rhs(bd);

%% Visualize
nrb.cval = coeffs;
figure; plot(nrb)

fprintf('L2 norm = %g\n', errornorm(nrb, uexact));

