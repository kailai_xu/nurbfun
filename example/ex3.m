% this script solves Poisson equation on the disk using Galerkin method
% see kailaix.wordpress.com/2018/04/02/solving-poisson-equation-using-galerkin-method/
NN = [];
err = [];
timer = [];
for num = [10 20 40 80 160]
    tic
    nrb = discnrb(num,num,3,3,2);
    f = @(x,y) 4 * ones(size(x));
    u = @(x,y) 4 - x.^2 - y.^2;
    
    A = nrb.aform('x','x') + nrb.aform('y','y');
    rhs = nrb.aform('u', f);
    
    bd = dirichletbd(nrb);
    nrb = nrb.fill(0);
    nrb.cval(~bd) = A(~bd, ~bd) \ rhs(~bd);
    err = [err errornorm(nrb, u)]
    timer = [timer toc];
    NN = [NN nrb.noBasis];
end
plot(nrb)