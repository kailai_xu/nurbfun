#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include "NURBS.h"
#include "mex.h"


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	/* numBasisFuns2D:

            N = NURBS2DBasisIndex(u, v, p, q, knotU, knotV) 

        return the index of basis functions for which they are nonzero at u,v.
        u,v can be vectors
    */
	if(nrhs != 6) mexErrMsgTxt("Syntax:\n"
        "N = NURBS2DBasisIndex(u, v, p, q, knotU, knotV) ");
			
	/* First get the inputs */
	
	double *u      = mxGetPr(prhs[0]);	
    double *v      = mxGetPr(prhs[1]);

    int    numPts = mxGetNumberOfElements(prhs[0]);
    mxAssert(mxGetNumberOfElements(prhs[0])==mxGetNumberOfElements(prhs[1]),"u and v do not match.");

	double *p_in    = mxGetPr(prhs[2]);
    double *q_in    = mxGetPr(prhs[3]);
    
	int    p        = (int) *p_in;
	int    q        = (int) *q_in;
    
	double *knotU   = mxGetPr(prhs[4]);
    double *knotV   = mxGetPr(prhs[5]);
    
	int    numKnotU = mxGetN(prhs[4]);
    int    numKnotV = mxGetN(prhs[5]);
    
	int    nU       = numKnotU - 1 - p - 1; // numBasisFunctions -1
    int    nV       = numKnotV - 1 - q - 1;
    int    noFuncs  = (p+1)*(q+1); 
	

	double tol    = 100*DBL_EPSILON;
	
    int *N = (int *) malloc(sizeof(int) * numPts * noFuncs);
    int k = 0;

    for (int i=0;i<numPts;i++){
        if(fabs(u[i]-knotU[numKnotU-1]) < tol) 
            u[i] = knotU[numKnotU-1] - tol;
    
        if(fabs(v[i]-knotV[numKnotV-1]) < tol) 
            v[i] = knotV[numKnotV-1] - tol; 

        int spanU = FindSpan(nU, p, u[i], knotU); 
        int spanV = FindSpan(nV, q, v[i], knotV); 

        
        for(int vind=spanV - q; vind <= spanV ; vind++)
            for(int uind=spanU - p; uind <= spanU ; uind++){
                N[k] = uind +  vind * (nU + 1);
                k ++;
            }


    }

    // mexPrintf("nU+1=%d, p=%d, q=%d\n", nU+1, p, q);

	plhs[0] = mxCreateNumericMatrix(numPts, noFuncs, mxINT32_CLASS, mxREAL);
    int* outputMatrix  = (int *)mxGetData(plhs[0]);
    for (int col=0; col < noFuncs; col++) {
        for (int row=0; row < numPts; row++) {
            outputMatrix[ row + col * numPts ] = N[col + row * noFuncs] + 1; // matlab index
        }
    }

    free(N);

}


 
 
 
 

