#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include "NURBS.h"
#include "mex.h"


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	/* Return the 2D NURBS basis functions and first/second derivatives to matlab
	   All non-zero basis functions and derivatives at point [xi,eta] are computed.
	
	  We expect the function to be called as 
	  [R dRdxi dRdeta dR2dxi dR2deta dR2dxideta] = NURBS2DBasis2ndDers(...
                                xi, p, q, knotU,knotV, weights)
	
		xi           = point, [xi eta], where we want to interpolate
		knotU, knotV = knot vectors
		weights      = vector of weights 
    
     Vinh Phu Nguyen, nvinhphu@gmail.com
    */
	
	if(nrhs != 6) mexErrMsgTxt("[R dRdxi dRdeta dR2dxi dR2deta dR2dxideta dR3dxi3 dR3dxi2deta dR3dxideta2 dR3dxideta3] = NURBS2DBasis3rdDers("
                                "xi, p, q, knotU,knotV, weights)\n");
			
	/* First get the inputs */
	
	double *xi      = mxGetPr(prhs[0]);	
	double *p_in    = mxGetPr(prhs[1]);
    double *q_in    = mxGetPr(prhs[2]);
    
	int    p        = (int) *p_in;
	int    q        = (int) *q_in;
    
	double *knotU   = mxGetPr(prhs[3]);
    double *knotV   = mxGetPr(prhs[4]);
    
	int    numKnotU = mxGetN(prhs[3]);
    int    numKnotV = mxGetN(prhs[4]);
    
	int    nU       = numKnotU - 1 - p - 1;
    int    nV       = numKnotV - 1 - q - 1;
    int    noFuncs  = (p+1)*(q+1); 
	
	double *weight  = mxGetPr(prhs[5]);
    int    numWeights = mxGetN(prhs[5]);
		
	double tol    = 100*DBL_EPSILON;
	
	if(fabs(xi[0]-knotU[numKnotU-1]) < tol) 
		xi[0] = knotU[numKnotU-1] - tol;
    
	if(fabs(xi[1]-knotV[numKnotV-1]) < tol) 
		xi[1] = knotV[numKnotV-1] - tol; 
	
	/* and evaluate the non-zero univariate B-spline basis functions
     * and first derivatives 
     */
	
	double *N      = (double *)malloc(sizeof(double)*(p+1));
    double *M      = (double *)malloc(sizeof(double)*(q+1));
	double **dersN = init2DArray(3+1, p+1);
    double **dersM = init2DArray(3+1, q+1);
	
	int spanU      = FindSpan(nU, p, xi[0], knotU); 
    int spanV      = FindSpan(nV, q, xi[1], knotV); 
    
	BasisFuns     (spanU, xi[0], p, knotU, N);
    BasisFuns     (spanV, xi[1], q, knotV, M);
    
	dersBasisFuns (spanU, xi[0], p, 3, knotU, dersN);	
    dersBasisFuns (spanV, xi[1], q, 3, knotV, dersM);
    
	/* and create NURBS approximation */
	
    int i, j, k, c;
    
    /*
    for(i=0;i<=p;i++){
        printf("dNdxi= %f\n", dersN[1][i]);
        printf("dNdet= %f\n", dersM[1][i]);
    }*/
    
    int uind = spanU - p;
    int vind;
    
    double w      = 0.0; /* w = N_I w_I*/
    double dw_xi  = 0.0; /* first derivative of w w.r.t xi*/
    double d2w_xi = 0.0; /* second derivative of w w.r.t xi*/
    double dw_eta  = 0.0; /* first derivative of w w.r.t eta*/
    double d2w_eta = 0.0; /* second derivative of w w.r.t eta*/
    double d2w_xieta = 0.0; /* second derivative of w w.r.t xi-eta*/

    /* third derivatives */
    double d3w_xi  = 0.0; 
    double d3w_xi2eta = 0.0; 
    double d3w_xieta2 = 0.0; 
    double d3w_eta = 0.0;

    double wi;
	
    for(j = 0; j <= q; j++)
    {
        vind = spanV - q + j;  
        
        for(i = 0; i <= p; i++)
        {               
            c   = uind + i + vind * (nU+1);            
            wi  = weight[c];
            
            w      += N[i]        * M[j] * wi;
            dw_xi  += dersN[1][i] * M[j] * wi;
            d2w_xi += dersN[2][i] * M[j] * wi;
            dw_eta  += dersM[1][j] * N[i] * wi;
            d2w_eta += dersM[2][j] * N[i] * wi;
            d2w_xieta += dersN[1][i] * dersM[1][j] * wi;

            d3w_xi += dersN[3][i] * dersM[0][j] * wi;
            d3w_xi2eta += dersN[2][i] * dersM[1][j] * wi;
            d3w_xieta2 += dersN[1][i] * dersM[2][j] * wi;
            d3w_eta += dersN[0][i] * dersM[3][j] * wi;
        }
    }

	/* create output */
    
    plhs[0] = mxCreateDoubleMatrix(1,noFuncs,mxREAL); 
    plhs[1] = mxCreateDoubleMatrix(1,noFuncs,mxREAL); 
    plhs[2] = mxCreateDoubleMatrix(1,noFuncs,mxREAL); 
    plhs[3] = mxCreateDoubleMatrix(1,noFuncs,mxREAL); 
    plhs[4] = mxCreateDoubleMatrix(1,noFuncs,mxREAL); 
    plhs[5] = mxCreateDoubleMatrix(1,noFuncs,mxREAL); 
    plhs[6] = mxCreateDoubleMatrix(1,noFuncs,mxREAL); 
    plhs[7] = mxCreateDoubleMatrix(1,noFuncs,mxREAL); 
    plhs[8] = mxCreateDoubleMatrix(1,noFuncs,mxREAL); 
    plhs[9] = mxCreateDoubleMatrix(1,noFuncs,mxREAL); 
    
    double *R      = mxGetPr(plhs[0]);
    double *dRdxi  = mxGetPr(plhs[1]); /* first derivative to xi*/
    double *dRdet  = mxGetPr(plhs[2]); /* first derivative to eta*/
    double *dR2dxi = mxGetPr(plhs[3]);
    double *dR2det = mxGetPr(plhs[4]);
    double *dR2dxe = mxGetPr(plhs[5]);

    double *dR3dxi3 = mxGetPr(plhs[6]);
    double *dR3dxi2deta = mxGetPr(plhs[7]);
    double *dR3dxideta2 = mxGetPr(plhs[8]);
    double *dR3dxideta3 = mxGetPr(plhs[9]);


    double g, g1, g2, g11, g12, g22, g111, g222, g112, g122;
    g = 1/w;
    g1 = -dw_xi/pow(w,2.);
    g2 = -dw_eta/pow(w,2.);
    g11 = -d2w_xi/pow(w,2.) + 2* pow(dw_xi,2.)/pow(w,3.);
    g12 = -d2w_xieta/pow(w,2.) + 2*dw_xi*dw_eta/pow(w,3.);
    g22 = -d2w_eta/pow(w,2.) + 2* pow(dw_eta,2.)/pow(w,3.);
    g111 = -d3w_xi/pow(w,2.) + 6*d2w_xi*dw_xi/pow(w,3.)  - 6*pow(dw_xi,3.)/pow(w,4.);
    g222 = -d3w_eta/pow(w,2.) + 6*d2w_eta*dw_eta/pow(w,3.)  - 6*pow(dw_eta,3.)/pow(w,4.);
    g112 = -d3w_xi2eta/pow(w,2.) + 2*d2w_xi*dw_eta/pow(w,3.) + 4 * dw_xi*d2w_xieta/pow(w,3.) - 6 * pow(dw_xi,2.) * dw_eta/pow(w,4.);
    g122 = -d3w_xieta2/pow(w,2.) + 2*d2w_eta*dw_xi/pow(w,3.) + 4 * dw_eta*d2w_xieta/pow(w,3.) - 6 * pow(dw_eta,2.) * dw_xi/pow(w,4.);

    
    uind = spanU - p;
    k    = 0;
    
    double fac;
    
    /*printf("uind= %d\n", uind);  
    printf("vind= %d\n", vind);  
    printf("nU+1= %d\n", nU+1);  */
    
    for(j = 0; j <= q; j++)
    {
        vind = spanV - q + j; 
        
        for(i = 0; i <= p; i++)
        {               
            c        = uind + i + vind*(nU+1);
            fac      = weight[c]/(w*w);
            wi       = weight[c];
            
            R[k]     = N[i]*M[j]*fac*w;
            
            dRdxi[k] = (dersN[1][i]*M[j]*w - N[i]*M[j]*dw_xi) * fac;
            dRdet[k] = (dersM[1][j]*N[i]*w - N[i]*M[j]*dw_eta) * fac;
            
            dR2dxi[k] = wi*(dersN[2][i]*M[j]/w - 2*dersN[1][i]*M[j]*dw_xi/w/w - N[i]*M[j]*d2w_xi/w/w + 2*N[i]*M[j]*dw_xi*dw_xi/w/w/w);
            dR2det[k] = wi*(dersM[2][j]*N[i]/w - 2*dersM[1][j]*N[i]*dw_eta/w/w - N[i]*M[j]*d2w_eta/w/w + 2*N[i]*M[j]*dw_eta*dw_eta/w/w/w);
            dR2dxe[k] = wi*(dersN[1][i]*dersM[1][j]/w - dersN[1][i]*M[j]*dw_eta/w/w - N[i]*dersM[1][j]*dw_xi/w/w - N[i]*M[j]*d2w_xieta/w/w + 2*N[i]*M[j]*dw_xi*dw_eta/w/w/w);
            
            dR3dxi3[k] = wi* (dersN[3][i] * dersM[0][j] * g + 3*dersN[2][i] * dersM[0][j]*g1 + 3*dersN[1][i] * dersM[0][j]*g11 +dersN[0][i]* dersM[0][j] *g111);
            dR3dxi2deta[k] = wi* ( dersN[2][i] * dersM[1][j] * g + 2*dersN[1][i] * dersM[1][j]*g1 + dersN[0][i] * dersM[1][j]*g11 + dersN[2][i] * dersM[0][j]*g2 + 2*dersN[1][i] * dersM[0][j]*g12 + dersN[0][i]* dersM[0][j] *g112 );
            dR3dxideta2[k] = wi* ( dersN[1][i] * dersM[2][j] * g + 2*dersN[1][i] * dersM[1][j]*g2 + dersN[1][i] * dersM[0][j]*g22 + dersN[0][i] * dersM[2][j]*g1 + 2*dersN[0][i] * dersM[1][j]*g12 + dersN[0][i]* dersM[0][j] *g122 );
            dR3dxideta3[k] = wi* ( dersN[0][i] * dersM[3][j] * g + 3*dersN[0][i] * dersM[2][j]*g2 + 3*dersN[0][i] * dersM[1][j]*g22 +dersN[0][i]* dersM[0][j] *g222 );


            k += 1;
        }
    }
    
      	/*mexPrintf("\nWe have a knot vector with %d components\n", numWeights);
				for(k = 0; k < numWeights; k++) mexPrintf("%2.2f\t", weight[k]);
				mexPrintf("\n");*/
                
	free(N);
    free(M);
	free2Darray(dersN, (3+1));
    free2Darray(dersM, (3+1));	
}


 
 
 
 

