# Welcome to NRBFUN

Inspired by many wonderful projects, to name a few, [chebfun](http://www.chebfun.org/), [igafem](https://sourceforge.net/projects/cmcodes/), [ifem](https://www.math.uci.edu/~chenlong/programming.html
), [NURBS for MATLAB](https://www.mathworks.com/matlabcentral/fileexchange/26390-nurbs-toolbox-by-d-m-spink) and many others, I decided to write an easy-to-hack, easy-to-use, easy-to-learn project for iso-geometric analysis. The iso-geometric analysis is becoming widely known nowadays and there are many nice projects. This project does not intend to compete with them but serves as a starting point for a fast prototype in research.

Just as many other basis functions, the programming related to NURBS basis needs several basic components:

* **Evaluation**. Point evaluations are basic and essential function parts for any basis functions.
* **Differentiation**. NURBS basis functions have the desirable property that it has continuous derivatives up to the order $\min(p,q)-1$. Up till now, 3rd order derivatives are supported.
* **Integration**. Some elementary integration routines are provided.
* **Degree of Freedom Mapping**

## Acknowledgement

The code uses portions of other open source projects. All credits go to the original authors. This code is only for research use.

## Setup

The setup can be done by the following code

```
>>> compile
>>> pathfinder
```

## Code Example
The following code solves the poisson equation 
$$-\Delta u(x) = f(x)$$
on the rectangle $[0,2]\times[0,2]$

```matlab
% This script demonstrate solving the Poisson problem 

clear all
close all
%% Make mesh
nrb = rectnrb(30);

%% PDE data
uexact = @(x,y) sin(pi*x) .* sin(pi*y);
f = @(x,y) 2*pi^2 * uexact(x,y);

%% Basic data 
noCtrlPts = prod(nrb.number);

uv = aveknt(nrb); % collocation points
u = uv{1}; v = uv{2};
[u,v] = meshgrid(u,v);
u = u(:);
v = v(:);

%% Assemble matrices
A = zeros(noCtrlPts);
rhs = zeros(noCtrlPts,1);

for i = 1:noCtrlPts
    N = nrb.index(u(i),v(i));
    [R, dR,ddR, coord, J] = nrb.phys2(u(i), v(i), N);
    A(i, N) =  - (ddR(1,:) + ddR(3,:)); % -\Delta 
    rhs(i) = f(coord(1),coord(2));        
end

%% apply boundary condition
bd = true(nrb.number);
bd(:,1) = false;
bd(:,end) = false;
bd(1,:) = false;
bd(end,:) = false; 
% this is the standard way to mark boundary condition

%% Solve
coeffs = zeros(noCtrlPts,1);
coeffs(bd) = A( bd, bd) \ rhs(bd);

%% Visualize
nrb.cval = coeffs;
figure; plot(nrb)

fprintf('L2 norm = %g\n', errornorm(nrb, uexact));

```

