set(0,'defaultLineLineWidth',2)
set(0,'defaultAxesFontSize',20)

addpath igafem-nurbs/bin
addpath octave-nurbs/inst
addpath tools
addpath tools/third-party
addpath example
addpath .
addpath test