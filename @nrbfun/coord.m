function out = coord(obj, u, v)
%COORD Return the physical coordinates at (u,v)
%   Detailed explanation goes here

N = obj.index(u,v);
R = obj.phys0(u,v,N);
out = R * obj.cpts(N,:);

end

