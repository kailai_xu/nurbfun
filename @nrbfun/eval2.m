function S = eval2(obj, u, v)
%EVAL2 Return the third derivatives at point u, v
%   Detailed explanation goes here
if obj.p-1<2 || obj.q-1<2
    warning('insufficient continuity of NURBS basis\nresult might be inaccurate');
end
N = obj.index(u,v);
[~,~,ddR, ~,~] = phys2(obj, u, v, N);
S = ddR * obj.cval(N);

end

