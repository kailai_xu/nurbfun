function S = eval3(obj, u, v)
%EVAL3 Return the third derivatives at point u, v
%   Detailed explanation goes here
if obj.p-1<3 || obj.q-1<3
    warning('insufficient continuity of NURBS basis\nresult might be inaccurate');
end
N = obj.index(u,v);
[~, ~,~,dddR, ~, ~] = phys3(obj, u, v, N);
S = dddR * obj.cval(N);

end

