function obj = nrbproject(obj, f)
%NRBINTERP Project function f onto nurbs space specified by obj.
%   nrb = nrb.nrbproject(f)

A = obj.aform0;
rhs = obj.lform0(f,5,'basis');

obj.cval = A\rhs;

obj.check_nrb_output(nargout)

end

