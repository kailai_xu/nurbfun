function [R, dR, coord, J] = phys1(obj, u, v, N)
%PHYS2 Return physical 2D 
%   Detailed explanation goes here
if nargin==3
    N = obj.index(u,v);
end
[R, dRdxi, dRdeta] = ...
    NURBS2DBasisDers([u, v], obj.p, obj.q, obj.knots{1},obj.knots{2}, obj.wgts); % derivatives in the parametric space
[R, dR, coord, J] = ders1st2d(obj.cpts(N,:), R, [dRdxi; dRdeta]); % convert to physical space
end

