function [R, dR,ddR,dddR, coord, J] = phys3(obj, u, v, N)
%PHYS3 Return physical 2D 
%   Detailed explanation goes here
if nargin==3
    N = obj.index(u,v);
end
[R, dRdxi, dRdeta, dR2dxi, dR2deta, dR2dxideta, dR3dxi3, dR3dxi2deta, dR3dxideta2, dR3dxideta3] = ...
    NURBS2DBasis3rdDers([u, v], obj.p, obj.q, obj.knots{1},obj.knots{2}, obj.wgts); % derivatives in the parametric space
[R, dR,ddR,dddR, coord, J] = ders3rd2d(obj.cpts(N,:), R, [dRdxi; dRdeta], ...
    [dR2dxi;dR2dxideta;dR2deta], [dR3dxi3; dR3dxi2deta; dR3dxideta2; dR3dxideta3]); % convert to physical space
end

