function s = errornorm(obj, f, type)
%ERRORNORM Returns the error norm of nrbfun and function f, f should be
%defined in the physical space.
if nargin==2
    type = 'L2';
end


switch type
    case 'L2'
        s = 0;
        for i = 1:obj.noCells
            u1 = obj.cells(i,1); 
            v1 = obj.cells(i,2);
            u2 = obj.cells(i,3);
            v2 = obj.cells(i,4);
            [x,y,w] = lgwt2(5, u1, v1, u2, v2);
            N = obj.index(mean(obj.cells(i,1:2)), mean(obj.cells(i,3:4)));
            for k = 1:length(x)
                [~, ~, coords, J] = obj.phys1(x(k), y(k), N);
                val = obj.eval(x(k), y(k));
                s = s + (f(coords(1), coords(2))-val)^2 * w(k) * J;
            end
        end
        s = sqrt(s);
    case {1, 2, 'inf', inf, 'fro', 'l2'}
        s = norm(obj - obj.nrbinterp(f), type);
    otherwise
        error(['operation type ' type ' not supported yet']);
end
end

