function S = eval1(obj, u, v)
%EVAL1 Return the third derivatives at point u, v
%   Detailed explanation goes here
if obj.p-1<1 || obj.q-1<1
    warning('insufficient continuity of NURBS basis\nresult might be inaccurate');
end
N = obj.index(u,v);
[~, dR, ~, ~] = phys1(obj, u, v, N);
S = dR * obj.cval(N);

end

