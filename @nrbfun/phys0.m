function [R, coord] = phys0(obj, u, v, N)
%PHYS0 Summary of this function goes here
%   Detailed explanation goes here
if nargin==3
    N = obj.index(u,v);
end
R = NURBS2DBasis([u, v], obj.p, obj.q, obj.knots{1},obj.knots{2}, obj.wgts); % derivatives in the parametric space
[R, coord] = ders2d(obj.cpts(N,:), R); % convert to physical space
end

