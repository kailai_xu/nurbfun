function B = eval(obj, u, v)
%EVAL Evalation of the nurb in the parametric space
%   Detailed explanation goes here

N = obj.index(u,v);
R = obj.phys0(u,v,N);
B = R * obj.cval(N);

end

