function [u, v, w, x, y, W] = nrbquad(obj, n)
%QUAD Generate quad points per cell
%   Detailed explanation goes here

if nargout ~= 3 && nargout ~= 6
    error('usage:\n[u, v, w, x, y, W] = nrbquad(obj, n)\n[u, v, w] = nrbquad(obj, n)');
end

u = zeros(n*n*obj.noCells,1);
v = zeros(n*n*obj.noCells,1);

index = 0;
for i = 1:obj.noCells
    [xx,yy,ww] = lgwt2(n, obj.cells(i,1), obj.cells(i,2), obj.cells(i,3), obj.cells(i,4));
    u(index+1:index+n^2) = xx;
    v(index+1:index+n^2) = yy;
    w(index+1:index+n^2) = ww;
    index = index + n^2;
end

if nargout==6
    x = zeros(n*n*obj.noCells,1);
    y = zeros(n*n*obj.noCells,1);
    W = zeros(n*n*obj.noCells,1);
    
    for i = 1:n^2*obj.noCells
        [~, ~, coord, J] = obj.phys1(u(i),v(i));
        x(i) = coord(1);
        y(i) = coord(2);
        W(i) = J * w(i);
    end
end

