function obj = nrbinterp(obj, f)
%NRBINTERP Interpolate function f onto nurbs space specified by obj.
%   nrb = nrb.nrbinterp(f)
%   nrb is given in the physical domain

noCtrlPts = prod(obj.number);
A = zeros(noCtrlPts);
rhs = zeros(noCtrlPts,1);

uv = aveknt(obj);
u = uv{1}; v = uv{2};
[u,v] = meshgrid(u,v);
u = u(:); v = v(:);

knotu = obj.knots{1};
knotv = obj.knots{2};
p = obj.order(1) - 1;
q = obj.order(2) - 1;
wgts = obj.wgts;
cpts = obj.cpts;

for i = 1:noCtrlPts
    N = NURBS2DBasisIndex(u(i), v(i), p, q, knotu, knotv);
    R = NURBS2DBasis([u(i),v(i)], p, q, knotu,knotv, wgts);
    [R, coord] = ders2d(cpts(N,:),R);
    A(i, N) =  R;
    rhs(i) = f(coord(1),coord(2));
end

obj.cval = A\rhs;
obj.check_nrb_output(nargout)

end

