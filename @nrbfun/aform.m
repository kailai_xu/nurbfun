function A = aform(obj,str1, str2, order)
%AFORM assembles the form
% (u_X, v_Y)
% where X, Y are derivatives specified by str1 and str2
% Notation list
% 1 -- constant h
% u -- no derivative
% x -- x derivative
% y -- y derivative
% xx -- x second derivative
% ...
%
% str can also be a function handle, which will be understood as
% (u_X, f) or (f, u_Y) depending on the position of f
%
% This function is very flexible and powerful

if nargin==3
    order = 5;
end

if strcmp(str1, '')
    str1 = @(x,y)ones(size(x));
end
if strcmp(str2, '')
    str2 = @(x,y)ones(size(x));
end

% Assign vector
if isa(str1, 'function_handle')
    s1 = 3;
elseif (ischar(str1) && strcmp(str1,'1'))
    s1 = 1;
else 
    s1 = 2;
end

if isa(str2, 'function_handle')
    s2 = 3;
elseif (ischar(str2) && strcmp(str2,'1'))
    s2 = 1;
else 
    s2 = 2;
end

if (s1 == 2 && s2 == 1) ||...
        (s1 == 3 && s2 == 1) || ...
        (s1 == 3 && s2 == 2)
    A = obj.aform(str2, str1);
    return
end

if (s1 == 1 && s2 == 1) || ...
        (s1 == 3 && s2 == 3)
    A = 0;
elseif (s1==1 && s2==2) || ...
        (s1==1 && s2==3) || ...
        (s1==2 && s2==3) 
    A = zeros(obj.noBasis,1);
else
    A = zeros(obj.noBasis);
end

if s1 ~= 3 && s2 == 3
    f = str2;
    str2 = 'f';
end

if s1 == 3 && s2 == 3
    f1 = str1;
    f2 = str2;
    str1 = 'f';
    str2 = 'f';
end

type = [lower(str1) '.' lower(str2)];

for i = 1:obj.noCells
    [x,y,w] = lgwt2(order, obj.cells(i,1), obj.cells(i,2), obj.cells(i,3), obj.cells(i,4));
    N = obj.index(mean(obj.cells(i,1:2)), mean(obj.cells(i,3:4)));
    for k = 1:length(x)
        
    %%% main assembling code : can be hacked to improve performance %%%
        switch type
            case 'u.u'
                [R, ~, ~, J] = obj.phys1(x(k), y(k), N);
                A(N, N) = A(N, N) + R' * R * w(k) * J;
            case '1.1'
                A = A + w(k) * J;
            case 'x.x' % tested
                [~, dR, ~, J] = obj.phys1(x(k), y(k), N);
                A(N, N) = A(N, N) + dR(1,:)' * dR(1,:) * w(k) * J;
            case 'y.y' % tested
                [~, dR, ~, J] = obj.phys1(x(k), y(k), N);
                A(N, N) = A(N, N) + dR(2,:)' * dR(2,:) * w(k) * J;
            case 'u.f' % tested
                [R, ~, coord, J] = obj.phys1(x(k), y(k), N);
                A(N) = A(N) + R' * f(coord(1), coord(2)) * w(k) * J;
            case 'x.f'
                [R, dR, coord, J] = obj.phys1(x(k), y(k), N);
                A(N) = A(N) + dR(1,:)' * f(coord(1), coord(2)) * w(k) * J;
            case 'y.f'
                [R, dR, coord, J] = obj.phys1(x(k), y(k), N);
                A(N) = A(N) + dR(2,:)' * f(coord(1), coord(2)) * w(k) * J;
            case 'f.f'
                [~, ~, coord, J] = obj.phys1(x(k), y(k), N);
                A = A + f2(coord(1), coord(2)) * f1(coord(1), coord(2)) * w(k) * J;
            otherwise
                error('not implemented yet');
        end
    %%% %%% %%% %%% %%% %%% %%% %%% %%% %%% %%% %%% %%% %%% %%% %%% %%%
        
    end
end



end

