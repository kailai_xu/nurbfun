function plot(obj)
%PLOT Plot the nrbfun object, using 100x100 grid
%   Detailed explanation goes here

x = linspace(0,1,5);
y = linspace(0,1,5);
[x,y] = meshgrid(x,y);
x = x(:);
y = y(:);
X = zeros(25,1);
Y = zeros(25,1);
Z = zeros(25,1);

hold on
if obj.noCells<50
    for i = 1:obj.noCells
        xx = obj.cells(i,1) + (obj.cells(i,2)-obj.cells(i,1)) * x;
        yy = obj.cells(i,3) + (obj.cells(i,4)-obj.cells(i,3)) * y;
        for k = 1:length(xx)
            coord = nrbeval(obj, {xx(k),yy(k)});
            X(k) = coord(1); Y(k) = coord(2);
            Z(k) = obj.eval(xx(k), yy(k));
        end
        N = sqrt(length(xx));
        surf( reshape(X,N,N), reshape(Y,N,N), reshape(Z,N,N));
    end
else
    x0 = linspace(0,1,11); x0 = x0(1:end-1); h = 0.1;
    [x0, y0] = meshgrid(x0,x0);
    x0 = x0(:); y0 = y0(:);
    for i = 1:length(x0)
        xx = x0(i) + h * x;
        yy = y0(i) + h * y;
        for k = 1:length(xx)
            coord = obj.coord(xx(k), yy(k));
            X(k) = coord(1); Y(k) = coord(2);
            Z(k) = obj.eval(xx(k), yy(k));
        end
        N = sqrt(length(xx));
        surf( reshape(X,N,N), reshape(Y,N,N), reshape(Z,N,N));
    end
end

xlabel('x');
ylabel('y');
shading interp
grid on
title(['number of control points = ' num2str(obj.noBasis)]);

end

