function out = lform0(obj, f, order, option)
%LFORM0 integrate ( N_i, f )
%   option -- 'basis' return numBasis x 1 matrix
%             default return (N,f)

if nargin==2
    order = 5;
    option = 'default';
elseif nargin==3
    option = 'default';
end

Ni = zeros(obj.noBasis,1);

for i = 1:obj.noCells
    [x,y,w] = lgwt2(order, obj.cells(i,1), obj.cells(i,2), obj.cells(i,3), obj.cells(i,4));
    N = obj.index(mean(obj.cells(i,1:2)), mean(obj.cells(i,3:4)));
    for k = 1:length(x)
        [R, ~, coords, J] = obj.phys1(x(k), y(k), N);
        Ni(N) = Ni(N) + f(coords(1), coords(2)) * R' * w(k) * J;
    end
end


if strcmp(option,'basis')
    out = Ni;
else
    out = Ni' * obj.cval;
end

