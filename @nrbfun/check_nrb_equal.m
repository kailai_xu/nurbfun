function check_nrb_equal(obj1, obj2)
%CHECK_NRB_EQUAL Summary of this function goes here
%   Detailed explanation goes here

props1 = properties(obj1);
props2 = properties(obj2);

for iprop = 1:length(props1)
    if strcmp(iprop,'cval')
        continue
    end
    p1 = props1{iprop};
    p2 = props2{iprop};
    s = all(p1==p2);
end

if ~s
    error('the nurbs in the two nrbfun instances are not equal');
end

end

