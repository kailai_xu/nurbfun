function [R, dR,ddR, coord, J] = phys2(obj, u, v, N)
%PHYS2 Return physical 2D 
%   Detailed explanation goes here
if nargin==3
    N = obj.index(u,v);
end
[R, dRdxi, dRdeta, dR2dxi, dR2deta, dR2dxideta] = ...
    NURBS2DBasis2ndDers([u, v], obj.p, obj.q, obj.knots{1},obj.knots{2}, obj.wgts); % derivatives in the parametric space
[R, dR, ddR, coord, J] = ders2nd2d(obj.cpts(N,:), R, [dRdxi; dRdeta], ...
    [dR2dxi;dR2dxideta;dR2deta]); % convert to physical space
end

