classdef nrbfun
    properties
        form
        dim
        number
        coefs
        knots
        order
        p
        q
        cval
        cpts
        wgts
        cells
        noCells
        noBasis
    end
    
    methods
        function obj = nrbfun(nrb,  vals)
            if length(nrb.order)~=2
                error('current nurbs type is not supported');
            end
            
            if nargin>1 && isnumeric(vals) && numel(vals) == prod(nrb.number)
                obj.cval = vals(:);
            end
            
            obj.form = nrb.form;
            obj.dim = nrb.dim;
            obj.number = nrb.number;
            obj.coefs = nrb.coefs;
            obj.knots = nrb.knots;
            obj.order = nrb.order;
            obj.p = obj.order(1)-1;
            obj.q = obj.order(2)-1;
            obj.cpts = [(nrb.coefs(1,:)./nrb.coefs(4,:))' (nrb.coefs(2,:)./nrb.coefs(4,:))'];
            obj.wgts = nrb.coefs(4,:)';
            
            obj.cells = zeros((length(obj.knots{1})-2*obj.p-1) * (length(obj.knots{2})-2*obj.q-1),4);
            k = 1;
            for j = obj.q+1:length(obj.knots{2})-obj.q-1
                for i = obj.p+1:length(obj.knots{1})-obj.p-1
                    u1 = obj.knots{1}(i);
                    v1 = obj.knots{1}(i+1);
                    u2 = obj.knots{2}(j);
                    v2 = obj.knots{2}(j+1);
                    obj.cells( k, : ) = [u1 v1 u2 v2];
                    k = k + 1;
                end
            end
            
            obj.noCells = (length(obj.knots{1})-2*obj.p-1) * (length(obj.knots{2})-2*obj.q-1);
            obj.noBasis = prod(obj.number);
        end
        
        
    end
    
    methods
        [R, coord] = phys0(obj, u, v, N)
        [R, dR, coord, J] = phys1(obj, u, v, N)
        [R, dR,ddR, coord, J] = phys2(obj, u, v, N)
        plot(obj)
        s = norm(obj,type)
        obj = nrbinterp(obj,f)
        out = lform0(obj, f, order, option)
        A = aform0(obj, order)
        obj = nrbproject(obj, f)
        S = eval(obj, u, v)
        S = eval1(obj, u, v)
        S = eval2(obj, u, v)
        S = eval3(obj, u, v)
        N = index(obj, u, v)
        obj = fill(obj, val)
        obj = check_nrb_output(obj,n)
        [u, v, w, x, y, W] = nrbquad(obj, n)
    end
    
    methods (Static)
        check_nrb_equal(obj1, obj2)
    end
    
    
    
end

