function s = norm(obj, type)
%NROM Summary of this function goes here
%   Detailed explanation goes here
if nargin == 1
    type = 2;
end

switch type
    case 'l2'
        s = norm(obj.cval)/sqrt(prod(obj.number));
    otherwise
        s = norm(obj.cval, type);
end

end

