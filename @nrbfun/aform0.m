function A = aform0(obj, order)
%AFORM0 integrate ( N_i, Nj ), a matrix is returned.

if nargin==1
    order = 5;
end

noBasis = prod(obj.number);
A = zeros(noBasis);

for i = 1:obj.noCells
    [x,y,w] = lgwt2(order, obj.cells(i,1), obj.cells(i,2), obj.cells(i,3), obj.cells(i,4));
    N = obj.index(mean(obj.cells(i,1:2)), mean(obj.cells(i,3:4)));
    for k = 1:length(x)
        [R, ~, ~, J] = obj.phys1(x(k), y(k), N);
        A(N, N) = A(N, N) + R' * R * w(k) * J;
    end
end