u = @(x,y) x.^3 .* y + y.^2;
ux = @(x,y) 3 * x.^2 .* y;
uy = @(x,y) x.^3 + 2 * y;
uxx = @(x,y) 6 * x .* y;
uxy = @(x,y) 3 * x.^2;
uyy = @(x,y) 2 * ones(size(x));
uxxx = @(x,y) 6 * y;
uxxy = @(x,y) 6 * x;
uxyy = @(x,y) zeros(size(x));
uyyy = @(x,y) zeros(size(x));

nrb = rectnrb(20);
nrb = nrb.nrbinterp( u);

x = rand;
y = rand;
u(2*x,2*y)
nrb.eval(x,y)

[ux(2*x,2*y) uy(2*x,2*y)]
nrb.eval1(x,y)'

[uxx(2*x,2*y) uxy(2*x,2*y) uyy(2*x,2*y)]
nrb.eval2(x,y)'

[uxxx(2*x,2*y) uxxy(2*x,2*y) uxyy(2*x,2*y) uyyy(2*x,2*y)]
nrb.eval3(x,y)'