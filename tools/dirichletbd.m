function bd = dirichletbd(nrb)
%DIRICHLETBD Return the diriclet boundary index in a bool vector
bd = false(nrb.number);
bd(:,1) = true;
bd(:,end) = true;
bd(1,:) = true;
bd(end,:) = true; 
bd = bd(:);
end

