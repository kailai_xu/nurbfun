function [X,Y,W] = lgwt2(N,u1,v1,u2,v2)

[x1,w1] = lgwt(N, u1,v1);
[x2,w2] = lgwt(N, u2,v2);

[X,Y] = meshgrid(x1,x2);
W = w1 * w2';
X = X(:);
Y = Y(:);
W = W(:);