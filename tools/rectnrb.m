function nrb = rectnrb(mresolution)
%RECTNRB Summary of this function goes here
%   Detailed explanation goes here
nrb = nrb4surf([0,0],[2,0],[0,2],[2,2]);
nrb = nrbdegelev(nrb,[3 3]);
a = linspace(0,1,mresolution);
nrb = nrbkntins(nrb, {a(2:end-1), a(2:end-1)});
nrb = nrbfun(nrb);
end

