function nrb = discnrb(numix, numiy, p, q, r)
% FL_NRB_DISC create a disc

if nargin == 4
    r = 1;
end
coef = zeros(4,3,3);
w = sqrt(2)/2;
coef(1:3,1,1) = [-w;-w;0];
coef(1:3,2,1) = [0;-1;0];
coef(1:3,3,1) = [w;-w;0];
coef(1:3,1,2) = [-1;0;0];
coef(1:3,2,2) = [0;0;0];
coef(1:3,3,2) = [1;0;0];
coef(1:3,1,3) = [-w;w;0];
coef(1:3,2,3) = [0;1;0];
coef(1:3,3,3) = [w;w;0];
coef(1:3,:,:) = coef(1:3,:,:)*r;
coef(4,:,:) = [1 w 1
               w 1 w
               1 w 1];
knots = {[0 0 0 1 1 1], [0 0 0 1 1 1]};
nrb = nrbmak(coef, knots);
knot1 = linspace(0,1,numix+1);
knot2 = linspace(0,1,numiy+1);
nrb = nrbdegelev(nrb, [p,q]-(nrb.order-1));

nrb = nrbkntins(nrb, {knot1(2:end-1) knot2(2:end-1)});
nrb = nrbfun(nrb);