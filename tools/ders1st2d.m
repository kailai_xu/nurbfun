function [R, dR, coord, J] = ders1st2d(cpts,R, dR)
% DERS1ST2D:  returns the physical function values and ders from parametric
% ones.
% 
% Calling Sequence:
% 
% [R, dR, coord, J] = ders1st2d(cpts,R, dR)
% 
%  INPUT:
%    
%    (in parametric space)
%    cpts - control points Nx2 array
%    R    - B values 1xN array
%    dR   - [dB/du; dB/dv] 2xN array
% 
%  OUTPUT:
% 
%    (in physical space)
%    R     - B values 1xN array (in ph
%    dR    - [dB/du; dB/dv] 2xN array
%    coord - physical coordinates
%    J     - Jacobian

%calculate the coordinates in the physical space
coord = R*cpts;

%calculate the Jacobian of the transformation
dxdxi = dR*cpts;

% Solve for first derivatives in global coordinates
dR = dxdxi\dR;

J = abs(det(dxdxi));

