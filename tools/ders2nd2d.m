function [R, dR, ddR, coord, J] = ders2nd2d(cpts,R, dR, ddR)
% DERS2ND2D computes the physical derivatives from parametrized ones
% cpts -- control points Nx2 array
% R -- basis function values 1xN array
% dR -- first order derivatives 2xN array
% ddR -- second order derivatives 3xN array
% 
% D1 -- dB/du
% D2 -- dB/dv
% Phi1 -- d2B/du2
% Phi2 -- d2B/dudv
% Phi3 -- d2B/dv2
% R1 -- dB/dx, dB/dy, 2xN array
% R2 -- d2B/dx2, d2B/dxdy, d2B/dy2, 3xN array 

tmp = cpts' * dR';
X1 = tmp(1,1); X2 = tmp(1,2); Y1 = tmp(2,1); Y2 = tmp(2,2);
tmp = cpts'*ddR';

X11 = tmp(1,1); X12 = tmp(1,2); X22 = tmp(1,3);
Y11 = tmp(2,1); Y12 = tmp(2,2); Y22 = tmp(2,3);
dR = [X1 X2; Y1, Y2]\dR;
ddR = [X1^2 2*X1*Y1 Y1^2
    X1*X2 X1*Y2+X2*Y1 Y1*Y2
    X2^2 2*X2*Y2 Y2^2]\ddR ...
- [X11 Y11
    X12 Y12
    X22 Y22]*dR;
coord = R*cpts;
J = abs(det([X1 X2; Y1, Y2]));