function [R, dR, ddR, dddR, coord, J] = ders3rd2d(cpts,R, dR, ddR, dddR)
% DERS3RD2D computes the physical derivatives from parametrized ones
% cpts -- control points Nex2 array
% R -- basis function values 1xNe array
% dR -- first order derivatives 2xNe array
% ddR -- second order derivatives 3xNe array
% dddR -- third order derivatives 4xNe array
% Here Ne is the number of nonzero basis functions in the certain element

tmp1 = cpts'*dR';
tmp2 = cpts'*ddR';
tmp3 = cpts'*dddR';

fu = tmp1(1,1);
fv = tmp1(1,2);
fuu = tmp2(1,1);
fuv = tmp2(1,2);
fvv = tmp2(1,3);
fuuu = tmp3(1,1);
fuuv = tmp3(1,2);
fuvv = tmp3(1,3);
fvvv = tmp3(1,4);

gu = tmp1(2,1);
gv = tmp1(2,2);
guu = tmp2(2,1);
guv = tmp2(2,2);
gvv = tmp2(2,3);
guuu = tmp3(2,1);
guuv = tmp3(2,2);
guvv = tmp3(2,3);
gvvv = tmp3(2,4);

%calculate the coordinates in the physical space
coord = R*cpts;

%calculate the Jacobian of the transformation
dxdxi = dR*cpts;

% Set up the second derivatives matrix and the matrix of squared first derivatives
d2xdxi2 = ddR*cpts;

dxdxi2 = [dxdxi(1,1)^2, 2*dxdxi(1,1)*dxdxi(1,2), dxdxi(1,2)^2;...
    dxdxi(1,1)*dxdxi(2,1), dxdxi(1,1)*dxdxi(2,2)+dxdxi(1,2)*dxdxi(2,1), dxdxi(1,2)*dxdxi(2,2);...
    dxdxi(2,1)^2, 2*dxdxi(2,1)*dxdxi(2,2), dxdxi(2,2)^2];

% Solve for first derivatives in global coordinates
dR = dxdxi\dR;

% Solve for second derivatives in global coordinates
ddR = dxdxi2\(ddR - d2xdxi2*dR);

dddR = [ fu^3 3*fu*gu 3*fu*gu^2 gu^3
    fu^2*fv fu^2*gv+2*gu*fu*fv gu^2*fv+2*gu*fu*gv gu^2*gv
    fv^2*fu fv^2*gu+2*gv*fv*fu gv^2*fu+2*gv*fv*gu gv^2*gu
    fv^3 3*fv^2*gv 3*fv*gv^2 gv^3] \ ( ...
    dddR - [3*fu*fuu 3*guu*fu+3*gu*fuu 3*guu*gu
    2*fu*fuv+fuu*fv 2*guv*fu+2*gu*fuv+fuu*gv+guu*fv 2*gu*guv+guu*gv
    2*fv*fuv+fvv*fu 2*guv*fv+2*gv*fuv+fvv*gu+gvv*fu 2*gv*guv+gvv*gu
    3*fv*fvv 3*gvv*fv+3*gv*fvv 3*gvv*gv]*ddR + ...
    [fuuu guuu
    fuuv guuv
    fuvv guvv
    fvvv gvvv]*dR);

J = abs(det(dxdxi));