% galerkin method

%% basic data
global alpha

alpha = 0.5;

nrb = discnrb(30,30,3,3,1);
A = zeros(nrb.noBasis);


nn = 20;
[txx, tyy, tww] = disk_rule(round(1.5*nn),round(1.5*nn),0,0,1.5);
[xx, yy, ww] = disk_rule(nn,nn,0,0,1.);

uvx = zeros(length(txx),2);
uvy = zeros(length(xx),2);
for i = 1:length(txx)
    coordx = [txx(i) tyy(i) 0];
    if norm(coordx)<1
        uvx(i,:) = nrbinverse(nrb, coordx,'Display',false);
    end
end

for i = 1:length(xx)
    coordy = [xx(i) yy(i) 0];
    uvy(i,:) = nrbinverse(nrb, coordy,'Display',false);
end

%% assemble right hand side
rhs = nrb.aform('u', @exact_sol5_f);


%% assemble the matrix
A = A + pi * integral(@(r) sigmah(r, 0.5)./r.^(2*alpha-1), 0, 0.5) * ...
    (nrb.aform('x','x') + nrb.aform('y','y'));
% A0 = A0 + pi * integral(@(r) sigmah(r, 0.5)./r.^(2*alpha-1), 0, 0.5) * ...
%     (nrb.aform(@exact_sol3_dx,'x') + nrb.aform(@exact_sol3_dy,'y'));

for i = 1:nrb.noCells
    [x,y,w] = lgwt2(5, nrb.cells(i,1), nrb.cells(i,2), nrb.cells(i,3), nrb.cells(i,4));
    N = nrb.index(mean(nrb.cells(i,1:2)), mean(nrb.cells(i,3:4)));
    for k = 1:length(x)
        [R, dR, coord, J] = nrb.phys1(x(k), y(k), N);
        A(N, N) = A(N, N) + R' * R * remConst(1, coord) * w(k) * J;
%         A0(N) = A0(N) + exact_sol3(coord(1), coord(2)) * R' * remConst(1, coord) * w(k) * J;
    end
end

%%

for i = 1:length(txx)
    [i, length(txx)]
%     profile on
    for j = 1:length(xx)
        coordx = [txx(i) tyy(i) 0];
        if norm(coordx)<1
            Nx = nrb.index(uvx(i,1), uvx(i,2));
            [Rx, dRx, ddRx, ~, Jx] = nrb.phys2(uvx(i,1), uvx(i,2), Nx);
        end
        
        coordy = [xx(j) yy(j) 0];
        Ny = nrb.index(uvy(j,1), uvy(j,2));
%         [Ry, dRy, ~, Jy] = nrb.phys1(uvy(j,1), uvy(j,2), Ny);
        [Ry, dRy, ddRy, ~, Jy] = nrb.phys2(uvy(j,1), uvy(j,2), Ny);
        
        v = coordx-coordy;
        scafac = 1/norm(v)^(2+2*alpha) * tww(i) * ww(j) * Jy * Jx;

        if norm(coordx)>=1
            A(Ny, Ny) = A(Ny, Ny) + Ry' * Ry * scafac - sigmah(norm(v), 0.5) * ...
                (dRy(1,:) * v(1) + dRy(2,:)*v(2))' * (dRy(1,:) * v(1) + dRy(2,:)*v(2)) * scafac;
%             A0(Ny) = A0(Ny) + exact_sol3(coordy(1), coordy(2)) * Ry' * scafac - sigmah(norm(v), 0.5) * ...
%                 (exact_sol3_dx(coordy(1), coordy(2)) * v(1) + exact_sol3_dy(coordy(1), coordy(2))*v(2)) * ...
%                 (dRy(1,:) * v(1) + dRy(2,:)*v(2) + ...
%                                 1/2 * ddRy(1,:) * v(1)^2 + ddRy(2,:) * v(1) * v(2) + 1/2 * ddRy(3,:) * v(2)^2)' * scafac;
        else
            A(Nx, Nx) = A(Nx, Nx) + Rx' * Rx * scafac;
            A(Nx, Ny) = A(Nx, Ny) - Rx' * Ry * scafac;
            A(Ny, Nx) = A(Ny, Nx) - Ry' * Rx * scafac;
            A(Ny, Ny) = A(Ny, Ny) + Ry' * Ry * scafac - sigmah(norm(v), 0.5) * ...
                (dRy(1,:) * v(1) + dRy(2,:)*v(2))' * (dRy(1,:) * v(1) + dRy(2,:)*v(2)) * scafac ...
                - sigmah(norm(v), 0.5) * (dRy(1,:) * v(1) + dRy(2,:)*v(2))' * (1/2 * ddRy(1,:) * v(1)^2 + ddRy(2,:) * v(1) * v(2) + 1/2 * ddRy(3,:) * v(2)^2) * scafac ...
                - sigmah(norm(v), 0.5) * (1/2 * ddRy(1,:) * v(1)^2 + ddRy(2,:) * v(1) * v(2) + 1/2 * ddRy(3,:) * v(2)^2)' * (dRy(1,:) * v(1) + dRy(2,:)*v(2)) * scafac;
%         if scafac>1e4
%             scafac
%         end
%             A0(Nx) = A0(Nx) + (exact_sol3(coordx(1), coordx(2)) - exact_sol3(coordy(1), coordy(2))) * Rx';
%             A0(Ny) = A0(Ny) + (exact_sol3(coordx(1), coordx(2)) - exact_sol3(coordy(1), coordy(2))) * (-Ry)' * scafac - sigmah(norm(v), 0.5) * ...
%                 (exact_sol3_dx(coordy(1), coordy(2)) * v(1) + exact_sol3_dy(coordy(1), coordy(2))*v(2)) * (dRy(1,:) * v(1) + dRy(2,:)*v(2) + ...
%                                 1/2 * ddRy(1,:) * v(1)^2 + ddRy(2,:) * v(1) * v(2) + 1/2 * ddRy(3,:) * v(2)^2)' * scafac;

        end
    end
%     profile viewer
%     pause
end
A = 2^(2*alpha) * gamma(1+alpha)/(pi*abs(gamma(-alpha))) * A / 2;

%% post-process

bd = dirichletbd(nrb);
nrb = nrb.fill(0);
nrb.cval(~bd) = A(~bd,~bd)\rhs(~bd);

%%
plot(nrb)
