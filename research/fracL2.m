% This runs simulations for C^{1,\alpha}
% L2 error and Linf error is computed for fractional Laplacian and exact
% solution
close all
clear all
global alpha
alpha = 0.75;

sigmar = 0.5;
CONST = 4 * integral2(@(x,y)sigmah(sqrt(x.^2+y.^2), sigmar)./(x.^2+y.^2).^(alpha),0,sigmar,0,sigmar);
CONST = CONST / 2;


nrb = discnrb(20,20,4,4,1.5);

% collocation points
uv = aveknt(nrb);
u = uv{1}; v= uv{2};
[u,v] = meshgrid(u,v);
u = u(:); v = v(:);

%index for storing entries of the LHS
A = zeros(nrb.noBasis);
colrhs = zeros(nrb.noBasis,1);
sampdof = false(nrb.noBasis,1);


[xx,yy,ww] = lgwt2(50, 0,1,0,1);
xx = xx(:);
yy = yy(:);
ww = ww(:);

for i = 1:nrb.noBasis
    [i, nrb.noBasis]
    N0 = nrb.index(u(i),v(i));
    [R0, dR0, ddR0, dddR0, coord0, ~] = nrb.phys3(u(i),v(i),N0);
    
    
    if norm(coord0)>=1
        A(i, N0) = R0;
        colrhs(i) = exact_sol3(coord0(1), coord0(2));
        continue
    end
    
    sampdof(i) = true;
    A(i, N0) = A(i, N0)-1/2*(ddR0(1,:) + ddR0(3,:)) * CONST + R0 * remConst(1.5,coord0);
    
    [~,colrhs(i)] = exact_sol3(coord0(1), coord0(2));
    
    for k = 1:length(xx)
        % integration utility
        
        N = nrb.index(xx(k), yy(k));
        [R, dR, coord, J] = nrb.phys1(xx(k),yy(k),N);
        
        v1 = coord(1)-coord0(1);
        v2 = coord(2)-coord0(2);
        r = norm(coord-coord0);
        scalefac = 1/r^(2+2*alpha)*J*ww(k);
        
        s = sigmah(r, sigmar);
        
        A(i, N0) = A(i, N0) + scalefac * R0 + ...
            s * scalefac * (dR0(1,:)*v1 + dR0(2,:)*v2 + ddR0(1,:)*v1^2/2 + ddR0(3,:)*v2^2/2 + ddR0(2,:)*v1*v2+...
            dddR0(1,:)*v1^3/6 + dddR0(4,:)*v2^3/6 + dddR0(2,:)*v1^2*v2/2 + dddR0(3,:)*v1*v2^2/2);
        A(i, N) = A(i, N) - scalefac * R;

    end
    %     test_galerkin - pi*1.5^2
end
A = 2^(2*alpha) * gamma(1+alpha)/(pi*abs(gamma(-alpha))) * A;
nrb.cval =  A \ colrhs;

% %% post processing
plot(nrb)
% 
% %%
% u = discnrb(10,10,3,3,1.5);
u = nrb;
u = u.nrbinterp(@exact_sol3);
plot(abs(u-nrb))
% 
% %%
errornorm(nrb, @exact_sol3, 'L2')


