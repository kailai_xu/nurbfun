% This runs simulations for C^{1,\alpha}
% L2 error and Linf error is computed for fractional Laplacian and exact
% solution
close all
clear all
global alpha
alpha = 0.75;

% demonstration of numerical quadrature:
% set alpha = 0.2 and run for [10,20,30,40,50]

DOF = [];
err = cell(4,1);
for numix = [10 20 30 40 50 60 70 80]
numiy = numix;
p = 4;
q = p;


tic
disp('Pre-processing...')

sigmar = 0.5;
CONST = 4 * integral2(@(x,y)sigmah(sqrt(x.^2+y.^2), sigmar)./(x.^2+y.^2).^(alpha),0,sigmar,0,sigmar);
CONST = CONST / 2;

nrb = discnrb(numix, numiy, p,q,1. + sigmar);
knotu = nrb.knots{1};
knotv = nrb.knots{2};

%the number of control points in the u and v directions
lenu = length(knotu)-p-1;  %number of basis functions in the u direction
lenv = length(knotv)-q-1;  %number of basis functions in the v direction
numnodes = lenu*lenv;      %number of control points

dim = 2; % we are on a two-dimensional world

ngaussx = p+1;
ngaussy = q+1;
ngaussedge = max(ngaussx,ngaussy);

%intialize the control net
coordinates = zeros(numnodes, dim+1);  %allocate one more column for the weights
coord_ij = zeros(numnodes, dim); %coordinate # -> i,j array
b_net = zeros(lenu, lenv, dim+1); %same array in the B_{ij} format

%initialize the arrays for the coordinates of the collocations
%sample points

num_samp = (lenu-2)*(lenv-2);
sample_points = zeros(num_samp, dim); %coordinates of the sample points in the interior
%coordinates and orientation of sample points on edges
sample_pts_left = zeros(lenv, dim+1);
sample_pts_right = zeros(lenv, dim+1);
sample_pts_bottom = zeros(lenu, dim+1);
sample_pts_top = zeros(lenu, dim+1);

sample_counter = 0; %counter for interior sample points
sample_counter_left = 0;
sample_counter_right = 0;
sample_counter_bottom = 0;
sample_counter_top = 0;


samp_span = zeros(num_samp, 2);

for j=1:lenv %for each node in the y direction
    
    %compute the sample points in the x-direction using Greville Abscisae
    coordy = sum(knotv(j+1:j+q))./q;
    
    for i=1:lenu % for each node in the x direction
        index = (j-1)*lenu + i; %the index of the coordinate array
        coordx = sum(knotu(i+1:i+p))./p;
        coordinates(index,:) = [nrb.coefs(1,i,j)./nrb.coefs(4,i,j), nrb.coefs(2,i,j)./nrb.coefs(4,i,j), nrb.coefs(4,i,j)]; %put the (i,j) node in the coordinate array
        b_net(i,j,:) = coordinates(index,:);
        coord_ij(index,:) = [i, j];
        
        
        if (i>1) && (i<lenu) && (j>1) && (j<lenv)
            sample_counter = sample_counter +1;
            sample_points(sample_counter,:) = [coordx, coordy];
            samp_span(sample_counter,:) = [i,j];
        end
        
        if i==1
            sample_counter_left = sample_counter_left+1;
            sample_pts_left(sample_counter_left,:) = [coordx, coordy, 2];
        end
        
        if i==lenu
            sample_counter_right = sample_counter_right + 1;
            sample_pts_right(sample_counter_right,:) = [coordx, coordy, 4];
        end
        
        if j==1
            sample_counter_bottom = sample_counter_bottom + 1;
            sample_pts_bottom(sample_counter_bottom,:) = [coordx, coordy, 1];
        end
        
        if j==lenv
            sample_counter_top = sample_counter_top + 1;
            sample_pts_top(sample_counter_top,:) = [coordx, coordy, 3];
        end
        
    end %for i
end %for j


[element_nod, element_int, span_elm] = getConnectivity2(knotu, knotv, p, q);
elementcounter = size(element_int,1);
nument = (p+1)*(q+1);

%establish sample point -> element connectivity
samp_elm = zeros(num_samp, 1);

for i=1:num_samp
    curu = sample_points(i,1);
    curv = sample_points(i,2);
    for t1 = 1:length(knotu)-1
        if (curu >= knotu(t1)) && (curu <= knotu(t1+1)) && (knotu(t1) < knotu(t1+1))
            for t2 = 1:length(knotv)-1
                if (curv >= knotv(t2)) && (curv <= knotv(t2+1)) && (knotv(t2) < knotv(t2+1))
                    samp_elm(i) = span_elm(t1,t2);
                end
            end
        end
    end
end

dirichlet = getBoundary2(numix, numiy, element_int);

toc
disp('Pre-computing B-Splines...')

%loop through the knot spans in each direction and pre-compute
%Bsplines and connectivity arrays

deriv_order = 3; %we need both derivatives for collocation
[ M_arr_u ] = makeIndexColBspline( knotu, p, deriv_order);
[ M_arr_v ] = makeIndexColBspline( knotv, q, deriv_order);



%assembly
toc
disp('Assembling linear system...')



%index for storing entries of the LHS
A = zeros(num_samp, lenu * lenv);
B = zeros(num_samp, lenu * lenv);
colrhs = zeros(num_samp,1);
coords = zeros(num_samp,2);


[xx, ww] = genGP_GW(16);
xx = [ (xx-1)/2 (xx+1)/2 ];
ww = [ ww/2, ww/2];

% xx = linspace(-1,1,20);
% xx = xx(2:end-1)';
% ww = 1/18 * ones(18,1);
uu = (xx+1)/2;

[ux,uy] = meshgrid(uu,uu);
[w1,w2] = meshgrid(ww/2,ww/2);
ux = ux(:);
uy = uy(:);
ww = w1(:).*w2(:);

Nquad = length(ux);
Rdata = cell(Nquad,1);
Cdata = cell(Nquad,1);
Jdata = cell(Nquad,1);
Condata = cell(Nquad,1);

for i = 1:length(ux)
    u = ux(i);
    v = uy(i);
    [N, dRdxi, dRdeta] = NURBS2DBasisDers([u,v], p, q, knotu, knotv, reshape(b_net(:,:,3), lenu*lenv,1));
    dN = [dRdxi;dRdeta];
    ni = find(u>=knotu,1, 'last');
    nj = find(v>=knotv,1, 'last');
    cpts = reshape(b_net(ni-p:ni, nj-q:nj, 1:2), nument, dim);
    [R, ~, coord, J] = getPhysicalDer1st(cpts, N, dN);
    Rdata{i} = R;
    Cdata{i} = coord;
    Jdata{i} = J;
    
    ni = ni - p;
    nj = nj - q;
    Condata{i} = element_nod(ni + (nj-1)*numix,:);
    
    ii = ni + (nj-1)*numix;
%     element_int(ni + (nj-1)*numiy,:)
%     [u,v]
end




% [ pt_index_u, M_arr_u1 ] = makeIndexBspline( knotu, p, 1, xx );
% [ pt_index_v, M_arr_v1 ] = makeIndexBspline( knotv, q, 1, xx );
% 
% Rdata = cell(elementcounter, length(xx), length(xx));
% Cdata = cell(elementcounter, length(xx), length(xx));
% Jdata = cell(elementcounter, length(xx), length(xx));
% 
% for ii = 1:elementcounter
%     for gp1 = 1:length(xx)
%         for gp2 = 1:length(xx)
%             ni = coord_ij(element_nod(ii,end),1);
%             nj = coord_ij(element_nod(ii,end),2);
%             t1 = pt_index_u(ni,gp1);
%             t2 = pt_index_v(nj,gp2);
%             wgts = reshape(b_net(ni-p:ni, nj-q:nj, dim+1), nument, 1);
%             cpts = reshape(b_net(ni-p:ni, nj-q:nj, 1:2), nument, dim);
%             M = M_arr_u1(:,:,t1);
%             P = M_arr_v1(:,:,t2);
%             
%             [N, dN]=nurbshape2d4(M, P, p, q, wgts);
%             [R, ~, coord, J] = getPhysicalDer1st(cpts,N, dN);
%             Rdata{ii,gp1,gp2} = R;
%             Cdata{ii,gp1,gp2} = coord;
%             Jdata{ii,gp1,gp2} = J;
%         end
%     end
% end



nrb = nrbfun(nrb);
sampdof = false(num_samp,1);
for sampcounter=1:num_samp
    %retrieve the element and span indexes
    i = samp_elm(sampcounter);
    t1 = samp_span(sampcounter, 1);
    t2 = samp_span(sampcounter, 2);
    
    
    %find the knot span index in each direction
    ni = coord_ij(element_nod(i,end),1);
    nj = coord_ij(element_nod(i,end),2);
    
    %calculate the weights and control points corresponding to the
    %current element
    
    wgts = reshape(b_net(ni-p:ni, nj-q:nj, dim+1), nument, 1);
    cpts = reshape(b_net(ni-p:ni, nj-q:nj, 1:2), nument, dim);
    
    M = M_arr_u(:,:,t1);
    P = M_arr_v(:,:,t2);
    
    [N, dN, ddN, dddN]=nurbshape2d6(M, P, p, q, wgts);
    [R0, dR0, ddR0, dddR0,   coord0, ~] = getPhysicalDers3rd(cpts,N, dN, ddN, dddN);
    
  
    
    conn0 = element_nod(i,:);
    coords(sampcounter,:) = coord0;
    
    
    B(sampcounter, conn0) = R0;
    if norm(coord0)>=1
        A(sampcounter, conn0) = R0;
        colrhs(sampcounter) = exact_sol3(coord0(1), coord0(2));
        continue
    end
    
    sampdof(sampcounter) = true;
    A(sampcounter, conn0) = A(sampcounter, conn0)-1/2*(ddR0(1,:) + ddR0(3,:)) * CONST + R0 * remConst(1.5,coord0);
    
    [~,colrhs(sampcounter)] = exact_sol3(coord0(1), coord0(2));
    
    DR = [dR0(1,:); dR0(2,:); ddR0(1,:); ddR0(3,:); ddR0(2,:); dddR0(1,:); dddR0(4,:); dddR0(2,:); dddR0(3,:)];
    
    for ii = 1:Nquad
        R = Rdata{ii};
        coord = Cdata{ii};
        J = Jdata{ii};
        conn = Condata{ii};
        v1 = coord(1)-coord0(1);
        v2 = coord(2)-coord0(2);
        r = norm(coord-coord0);
        scalefac = 1/r^(2+2*alpha)*J*ww(ii);
        
        s = sigmah(r, sigmar);
        
        A(sampcounter, conn0) = A(sampcounter, conn0) + scalefac * R0 + ...
            s * scalefac *  (DR' * [v1 ;v2 ;v1^2/2 ;v2^2/2 ;v1*v2 ;
            v1^3/6; v2^3/6; v1^2*v2/2; v1*v2^2/2])';
        A(sampcounter, conn) = A(sampcounter, conn) - scalefac * R;
    end
        
    
%     for ii = 1:elementcounter
%         for gp1 = 1:length(xx)
%             for gp2 = 1:length(xx)
%                 
%                 R = Rdata{ii,gp1,gp2};
%                 coord = Cdata{ii,gp1,gp2};
%                 J = Jdata{ii,gp1,gp2};
%                 
%                 
%                 conn = element_nod(ii,:);
%                 
%                 v1 = coord(1)-coord0(1);
%                 v2 = coord(2)-coord0(2);
%                 r = norm(coord-coord0);
%                 scalefac = 1/r^(2+2*alpha)*J*ww(gp1)*ww(gp2)*(element_int(ii,3) -element_int(ii,1))/2 *(element_int(ii,4) -element_int(ii,2))/2;
%                 
%                 s = sigmah(r, sigmar);
%                 
%                 A(sampcounter, conn0) = A(sampcounter, conn0) + scalefac * R0 + ...
%                     s * scalefac *  (DR' * [v1 ;v2 ;v1^2/2 ;v2^2/2 ;v1*v2 ;
%                     v1^3/6; v2^3/6; v1^2*v2/2; v1*v2^2/2])';
%                 A(sampcounter, conn) = A(sampcounter, conn) - scalefac * R;
%                 
%                  
%             end
%         end
%     end
    %     test_galerkin - pi*1.5^2
end

A = 2^(2*alpha) * gamma(1+alpha)/(pi*abs(gamma(-alpha))) * A;

% z0 = B\exact_sol3(coords(:,1), coords(:,2));
% I = coords(:,1).^2+coords(:,2).^2<1;
% y0 = A*z0;
% figure;
% scatter3(coords(I,1), coords(I,2), y0(I))
% hold on
% [~,dz0] = exact_sol3(coords(I,1), coords(I,2));
% scatter3(coords(I,1), coords(I,2), dz0 ,'*')

z0 = B\exact_sol3(coords(:,1), coords(:,2));
I = coords(:,1).^2+coords(:,2).^2<1;
y0 = A*z0;
[~,dz0] = exact_sol3(coords(I,1), coords(I,2));
errw1inf = norm(y0(I) - dz0,'inf');
errw1l2 = sqrt(mean((y0(I) - dz0).^2));

toc
disp('Imposing Dirichlet boundary conditions...')
[bcdof] = DirichletProj(dirichlet, element_nod, numnodes, knotu, knotv, ngaussedge, nument, b_net, p, q, lenu, lenv, coord_ij, coordinates);
bcval = zeros(size(bcdof));


%impose the Dirichlet boundary conditions
red_vect = zeros(1, numnodes);
red_vect(bcdof) = bcval;

colrhs = colrhs - A*red_vect';
A(:, bcdof) = [];

toc

disp('Solving the linear system...')
% condition_number_estimate = condest(A);
% fprintf('Condition number = %f\n', condition_number_estimate);
sol0 = A\colrhs;


[bcdofs, ind] = sort(bcdof);
bcvals = bcval(ind);
for i=1:length(bcdof)
    sol0=[sol0(1:bcdofs(i)-1); bcvals(i); sol0(bcdofs(i):end)];
end
toc
fprintf(' p=%d , num_intervals=%d, num_dof = %d\n', p, numix, size(A,1))

% plotsolGlobalCol(sol0, elementcounter, p, q, knotu, knotv, b_net, lenu, lenv, element_nod, coord_ij)
% hold on
% a = linspace(-1.5,1.5,40);
% [X,Y] = meshgrid(a,a);
% X = X(:); Y= Y(:);
% I = X.^2+Y.^2<1.5^2;
% X = X(I);Y = Y(I);
% z1 = exact_sol3(X,Y);
% scatter3(X,Y,z1,'*r')

[errw0inf,errw0l2] = myCalcinfnorm(sol0, element_int, elementcounter, p, q, knotu, knotv, b_net, element_nod, coord_ij, nument, @exact_sol3);

fprintf('DOF\terrw0inf\terrw0l2\terrw1inf\terrw1l2\n%g\t%g\t%g\t%g\t%g\n', ...
    size(A,1),errw0inf,errw0l2,errw1inf,errw1l2);
err{1} = [err{1} errw0inf];
err{2} = [err{2} errw0l2];
err{3} = [err{3} errw1inf];
err{4} = [err{4} errw1l2];
DOF = [DOF size(A,1)];

end

showrate(DOF, err{2});