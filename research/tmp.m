global alpha
alpha = 0.5;
nrb = discnrb( 10, 10, 3, 3, 1);

rhs0 = nrb.aform(@exact_sol5_f, @exact_sol5);
A0 = 0;

%%

[uu,vv,ww, xx,yy,xyw] = nrb.nrbquad(3);


%%
A0 = A0 + pi * integral(@(r) sigmah(r, 0.5) ./ r.^(2*alpha-1),0,0.5) * ...
    (nrb.aform(@exact_sol3_dx,@exact_sol3_dx) + nrb.aform(@exact_sol3_dy,@exact_sol3_dy));

for i = 1:nrb.noCells
    [x,y,w] = lgwt2(5, nrb.cells(i,1), nrb.cells(i,2), nrb.cells(i,3), nrb.cells(i,4));
    N = nrb.index(mean(nrb.cells(i,1:2)), mean(nrb.cells(i,3:4)));
    for k = 1:length(x)
        [R, dR, coord, J] = nrb.phys1(x(k), y(k), N);
        A0 = A0 + exact_sol3(coord(1), coord(2))^2 * remConst(1.0, coord) * w(k) * J;
        A0 = A0 + pi/alpha * exact_sol3(coord(1), coord(2))^2 * w(k) * J;
    end
end

%%
for i = 1:length(xx)
    [i, length(xx)]
    coordx = [xx(i), yy(i)];
    for j = 1:length(xx)
        coordy = [xx(j) yy(j)];
        v = coordx - coordy;
        
        if norm(v)<1e-5
            continue
        end
        scafac = 1/norm(v)^(2+2*alpha) * ww(i) * ww(j) * Jy * Jx;
        s = sigmah(norm(v), 0.5);
        

        ux = exact_sol3(coordx(1), coordx(2)); uy = exact_sol3(coordy(1), coordy(2));
        dudx = exact_sol3_dx(coordy(1), coordy(2)); dudy = exact_sol3_dy(coordy(1), coordy(2));
        dudxdx = exact_sol3_dxdx(coordy(1), coordy(2)); dudxdy = exact_sol3_dxdy(coordy(1), coordy(2)); 
        dudydy = exact_sol3_dydy(coordy(1), coordy(2));
        A0 = A0 + scafac * (ux-uy)^2 - scafac * s * ( dudx * v(1) + dudy * v(2) )^2 - ...
            2 * scafac * s * (dudx * v(1) + dudy * v(2)) * ( 1/2 * dudxdx * v(1)^2 + dudxdy * v(1) * v(2) + 1/2 * dudydy * v(2)^2);
    end
    
end
A0 = 2^(2*alpha) * gamma(1+alpha)/(pi*abs(gamma(-alpha))) * A0 / 2;

% bd = dirichletbd(nrb);
% nrb = nrb.fill(0);
% nrb.cval(~bd) = A(~bd,~bd)\rhs(~bd);

%%
% plot(nrb)

