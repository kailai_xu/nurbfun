function z = remConst(R,c)
% add up the values outside the reference
% kernel 1/|x-y|^{2+2\alpha}
% domain disc

global alpha

r = norm(c);
z = 1/(2*alpha) * integral(@(t) 1./(r*cos(t) + sqrt(R^2-r^2*sin(t).^2)).^(2*alpha),0,2*pi);


