function [z,dz] = exact_sol4(X,Y)
global alpha

t = X.^2 + Y.^2;

% z = exp(-5*t);
z = (t<1).*(1-t).^(alpha);
dz = (t<1).*(2^(2*alpha) * gamma(alpha+1) * gamma((2+2*alpha)/2));
end