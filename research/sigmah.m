function y = sigmah(r, h)
% SIGMAH computes the window function
% if h is not specified, h = 1

if nargin==1
    h = 1;
end

x = r/h;
y = (1-35*x.^4 + 84*x.^5 - 70*x.^6 + 20*x.^7).*(x<1).*(x>=0);