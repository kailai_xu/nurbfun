% This runs simulations for C^{1,\alpha}
% L2 error and Linf error is computed for fractional Laplacian and exact
% solution
close all
clear all
global alpha
alpha = 0.75;
err = [];
for num = [10 20 30 40 50]
sigmar = 0.5;
CONST = 4 * integral2(@(x,y)sigmah(sqrt(x.^2+y.^2), sigmar)./(x.^2+y.^2).^(alpha),0,sigmar,0,sigmar);
CONST = CONST / 2;

nrb = discnrb(num, num,4,4,1.5);

% collocation points
uv = aveknt(nrb);
u = uv{1}; v= uv{2};
[u,v] = meshgrid(u,v);
u = u(:); v = v(:);

%index for storing entries of the LHS



[xx,yy,ww] = lgwt2(16, 0,0.5,0,0.5);
xx = [xx
    xx
    xx + 0.5
    xx + 0.5];
yy = [yy
    yy + 0.5
    yy
    yy + 0.5];
ww = [ww
    ww
    ww
    ww];


% test use
sample_points = zeros((nrb.number(1)-2)*(nrb.number(2)-2), 2); 
sample_counter = 0;
for j=1:nrb.number(2) %for each node in the y direction
    
    %compute the sample points in the x-direction using Greville Abscisae
    coordy = sum(nrb.knots{2}(j+1:j+nrb.q))./nrb.q;
    
    for i=1:nrb.number(1) % for each node in the x direction
        index = (j-1)*nrb.number(1) + i; %the index of the coordinate array
        coordx = sum(nrb.knots{1}(i+1:i+nrb.p))./nrb.p;
        
        
        if (i>1) && (i<nrb.number(1)) && (j>1) && (j<nrb.number(2))
            sample_counter = sample_counter +1;
            sample_points(sample_counter,:) = [coordx, coordy];
        end
        
        
    end %for i
end %for j
A = zeros(size(sample_points,1),nrb.noBasis);
colrhs = zeros(size(sample_points,1),1);


data.J = cell(length(xx),1);
data.R = cell(length(xx),1);
data.N = cell(length(xx),1);
data.coord = cell(length(xx),1);

for k = 1:length(xx)
    N = nrb.index(xx(k), yy(k));
    [R, dR, coord, J] = nrb.phys1(xx(k),yy(k),N);
    data.J{k} = J;
    data.R{k} = R;
    data.N{k} = N;
    data.coord{k} = coord;
end

for i = 1:size(sample_points,1)
%     [i, nrb.noBasis]
    N0 = nrb.index(sample_points(i,1),sample_points(i,2));
    [R0, dR0, ddR0, dddR0, coord0, ~] = nrb.phys3(sample_points(i,1),sample_points(i,2),N0);
    
    
    if norm(coord0)>=1
        A(i, N0) = R0;
        colrhs(i) = exact_sol4(coord0(1), coord0(2));
        continue
    end
    
    A(i, N0) = A(i, N0)-1/2*(ddR0(1,:) + ddR0(3,:)) * CONST + R0 * remConst(1.+sigmar,coord0);
    
    [~,colrhs(i)] = exact_sol4(coord0(1), coord0(2));
    DR = [dR0(1,:); dR0(2,:); ddR0(1,:); ddR0(3,:); ddR0(2,:); dddR0(1,:); dddR0(4,:); dddR0(2,:); dddR0(3,:)];
    
    for k = 1:length(xx)
        % integration utility
        v1 = data.coord{k}(1)-coord0(1);
        v2 = data.coord{k}(2)-coord0(2);
        r = norm(data.coord{k}-coord0,2);
        scalefac = 1/r^(2+2*alpha)*data.J{k}*ww(k);
        
        s = sigmah(r, sigmar);
        
        A(i, N0) = A(i, N0) + scalefac * R0 + ...
            s * scalefac *  (DR' * [v1 ;v2 ;v1^2/2 ;v2^2/2 ;v1*v2 ;
            v1^3/6; v2^3/6; v1^2*v2/2; v1*v2^2/2])';
        A(i, data.N{k}) = A(i, data.N{k}) - scalefac * data.R{k};

    end
    %     test_galerkin - pi*1.5^2
end
A = 2^(2*alpha) * gamma(1+alpha)/(pi*abs(gamma(-alpha))) * A;

bd = true(nrb.number);
bd(:,1) = false;
bd(:,end) = false;
bd(1,:) = false;
bd(end,:) = false; 
bd = bd(:);

nrb.cval = zeros(nrb.noBasis,1);
A = A(:,bd);
nrb.cval(bd) =  A \ colrhs;

% nrb.cval = A\colrhs;
% %% post processing
% plot(nrb)
% 
% %%
% u = discnrb(10,10,3,3,1.5);
% u = nrb;
% u = u.nrbinterp(@exact_sol4);
% plot(abs(u-nrb))
% 
% %%
err = [err errornorm(nrb, @exact_sol4, 'L2')]
end


