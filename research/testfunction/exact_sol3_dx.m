function dx = exact_sol3_dx(X,Y)
global alpha

t = X.^2 + Y.^2;


dx = (t<1) .* (-2*X) .* (1-t).^alpha * (alpha+1);

end