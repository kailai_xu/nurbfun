function dz = exact_sol3_f(X,Y)
global alpha

t = X.^2 + Y.^2;

% z = exp(-5*t);
dz = (t<1).*(2^(2*alpha) * gamma(alpha+2) * gamma((2+2*alpha)/2) * (1-(1+alpha)*t));
end