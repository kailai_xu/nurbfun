function dy = exact_sol3_dy(X,Y)
global alpha

t = X.^2 + Y.^2;

dy = (t<1) .* (-2*Y) .* (1-t).^alpha * (alpha+1);
end