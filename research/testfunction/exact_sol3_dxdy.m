function dy = exact_sol3_dxdy(X,Y)
global alpha

t = X.^2 + Y.^2;

dy = alpha * (1+alpha) * (t<1) .* 4 * X.*Y .*(1-t).^(alpha-1);
end