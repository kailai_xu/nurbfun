function dy = exact_sol3_dxdx(X,Y)
global alpha

t = X.^2 + Y.^2;

dy = (t<1) .* ...
    ( -2 * (1+alpha) * (1-t).^alpha + 4*(1+alpha)*alpha*X.^2.*(1-t).^(alpha-1)  );
end