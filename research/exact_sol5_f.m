function dz = exact_sol5_f(X,Y)
% EXACT_SOL4 very smooth function in R^2
% Larger e indicates faster decay.

global alpha
e = 3;

X = e* X;
Y = e* Y;
t = X.^2 + Y.^2;


% dz = -(- 4 * exp(-t) + 4* t.*exp(-t));

dz = e^(2*alpha) * arrayfun( @helper, sqrt(t));


%     function y = helper(x)
%         y = 1/(2*pi) / 2 * integral2(@(r,t) r.^(2*alpha+1) .* exp(-r.^2/4) .* cos(r.*cos(t) * x), ...
%             0, 10, 0, 2*pi);
%     end


    function tmp = helper(x)
        [tt, ww] = GaussLaguerre(20,alpha);
        tt2 = linspace(0,2*pi,20);
        tt2 = tt2(1:end-1);
        ww2 = 2*pi/length(tt2) * ones(size(tt2));
        [TT1, TT2] = meshgrid(tt, tt2);
        WW = ww * ww2;
        WWT = WW';
        
        tmp = sum(2^(2*alpha+1) * cos(2*x*sqrt(TT1(:)).*sin(TT2(:))) .* WWT(:)) / (4*pi);
%         tmp = 0;
%         for i = 1:length(tt)
%             tmp = tmp + 2^(2*alpha+1) * integral(@(theta) cos(2*x*sqrt(tt(i))*sin(theta)), 0, 2*pi) * ww(i);
%         end
%         tmp = tmp / (4*pi);
    end

end