% This runs simulations for C^{1,\alpha}
% L2 error and Linf error is computed for fractional Laplacian and exact
% solution
close all
clear all
global alpha
alpha = 0.75;

sigmar = 0.1;
CONST = 4 * integral2(@(x,y)sigmah(sqrt(x.^2+y.^2), sigmar)./(x.^2+y.^2).^(alpha),0,sigmar,0,sigmar);
CONST = CONST / 2;


nrb = discnrb(10,10,3,3,1.5);

% collocation points
uv = aveknt(nrb);
u = uv{1}; v= uv{2};
[u,v] = meshgrid(u,v);
u = u(:); v = v(:);

%index for storing entries of the LHS
A = zeros(nrb.noBasis);
colrhs = zeros(nrb.noBasis,1);
coords = zeros(nrb.noBasis,2);
sampdof = false(nrb.noBasis,1);


for i = 1:nrb.noBasis
    [i, nrb.noBasis]
    N0 = nrb.index(u(i),v(i));
    [R0, dR0, ddR0, dddR0, coord0, ~] = nrb.phys3(u(i),v(i),N0);
    coords(i,:) = coord0;
    
    
    if norm(coord0)>=1
        A(i, N0) = R0;
        colrhs(i) = exact_sol3(coord0(1), coord0(2));
        continue
    end
    
    sampdof(i) = true;
    A(i, N0) = A(i, N0)-1/2*(ddR0(1,:) + ddR0(3,:)) * CONST + R0 * remConst(1.5,coord0);
    
    [~,colrhs(i)] = exact_sol3(coord0(1), coord0(2));
    
    for ii = 1:nrb.noCells
        % integration utility
        [xx,yy,ww] = lgwt2(5, nrb.cells(ii,1), nrb.cells(ii,2), nrb.cells(ii,3), nrb.cells(ii,4));
        N = nrb.index(mean(nrb.cells(ii,1:2)), mean(nrb.cells(ii,3:4)));
        for k = 1:length(xx)

                [R, dR, coord, J] = nrb.phys1(xx(k),yy(k),N);
                
                v1 = coord(1)-coord0(1);
                v2 = coord(2)-coord0(2);
                r = norm(coord-coord0);
                scalefac = 1/r^(2+2*alpha)*J*ww(k);
                
                s = sigmah(r, sigmar);
                
                A(i, N0) = A(i, N0) + scalefac * R0 + ...
                    s * scalefac * (dR0(1,:)*v1 + dR0(2,:)*v2 + ddR0(1,:)*v1^2/2 + ddR0(3,:)*v2^2/2 + ddR0(2,:)*v1*v2+...
                    dddR0(1,:)*v1^3/6 + dddR0(4,:)*v2^3/6 + dddR0(2,:)*v1^2*v2/2 + dddR0(3,:)*v1*v2^2/2);
                A(i, N) = A(i, N) - scalefac * R;

            
        end
    end
    %     test_galerkin - pi*1.5^2
end
A = 2^(2*alpha) * gamma(1+alpha)/(pi*abs(gamma(-alpha))) * A;
nrb.cval =  A \ colrhs;

% %% post processing
plot(nrb)
% 
% %%
% u = discnrb(10,10,3,3,1.5);
% u = u.nrbinterp(@exact_sol3)
% 
% %%
% errornorm(nrb, @exact_sol3, 'L2')


