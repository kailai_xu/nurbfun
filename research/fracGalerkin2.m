global alpha
alpha = 0.5;
nrb = discnrb( 10, 10, 3, 3, 1);

A = zeros(nrb.noBasis);
rhs = nrb.aform('u', @exact_sol3_f);

% rhs0 = nrb.aform(@exact_sol3_f, @exact_sol3);
% A0 = 0;

%%


nrb2 = discnrb( 5, 5, 3, 3, 1.5);
[u2,v2,wy, txx,tyy,tww] = nrb2.nrbquad(3);
uvx = [u2 v2];

nrb1 = discnrb( 5, 5, 3, 3, 1.);
[u1,v1,wx, xx,yy,ww] = nrb1.nrbquad(3);
uvy = [u1 v1];


%%
A = A + pi * integral(@(r) sigmah(r, 0.5) ./ r.^(2*alpha-1),0,0.5) * ...
    (nrb.aform('x','x') + nrb.aform('y','y'));
% A0 = A0 + pi * integral(@(r) sigmah(r, 0.5) ./ r.^(2*alpha-1),0,0.5) * ...
%     (nrb.aform(@exact_sol3_dx,@exact_sol3_dx) + nrb.aform(@exact_sol3_dy,@exact_sol3_dy));

for i = 1:nrb.noCells
    [x,y,w] = lgwt2(5, nrb.cells(i,1), nrb.cells(i,2), nrb.cells(i,3), nrb.cells(i,4));
    N = nrb.index(mean(nrb.cells(i,1:2)), mean(nrb.cells(i,3:4)));
    A_local = zeros(length(N));
    for k = 1:length(x)
        [R, dR, coord, J] = nrb.phys1(x(k), y(k), N);
        A_local = A_local + R' * R * remConst(1.0, coord) * w(k) * J;
%         A0 = A0 + exact_sol3(coord(1), coord(2))^2 * remConst(1.0, coord) * w(k) * J;
    end
    A(N,N) = A(N,N) + A_local;
end

%%
for i = 1:length(txx)
    [i, length(txx)]
    coordx = [txx(i), tyy(i)];
    if norm(coordx)<1
        Nx = nrb.index(uvx(i,1), uvx(i,2));
        [Rx, dRx, ddRx, ~, Jx] = nrb.phys2(uvx(i,1), uvx(i,2), Nx);
    end
    
    for j = 1:length(xx)
        coordy = [xx(j) yy(j)];
        Ny = nrb.index(uvy(j,1), uvy(j,2));
        [Ry, dRy, ddRy, ~, Jy] = nrb.phys2(uvy(j,1), uvy(j,2), Ny);
        
        v = coordx - coordy;
        scafac = 1/norm(v)^(2+2*alpha) * tww(i) * ww(j) * Jy * Jx;
        s = sigmah(norm(v), 0.5);
        
        if norm(coordx)<1
            A(Nx, Nx) = A(Nx, Nx) + Rx' * Rx * scafac;
            A(Nx, Ny) = A(Nx, Ny) - Rx' * Ry * scafac;
            A(Ny, Nx) = A(Ny, Nx) - Ry' * Rx * scafac;
            
        end
        A(Ny, Ny) = A(Ny, Ny) + Ry' * Ry * scafac ...
            - s * scafac * ( dRy(1,:) * v(1) + dRy(2,:) * v(2))' * ( dRy(1,:) * v(1) + dRy(2,:) * v(2)) ...
            - s * scafac * ( 1/2 * ddRy(1,:) * v(1)^2 + ddRy(2,:) * v(1) * v(2) + 1/2 * ddRy(3,:) * v(2)^2)' * ( dRy(1,:) * v(1) + dRy(2,:) * v(2)) ...
            - s * scafac * ( dRy(1,:) * v(1) + dRy(2,:) * v(2))' * ( 1/2 * ddRy(1,:) * v(1)^2 + ddRy(2,:) * v(1) * v(2) + 1/2 * ddRy(3,:) * v(2)^2);
        
%         ux = exact_sol3(coordx(1), coordx(2)); uy = exact_sol3(coordy(1), coordy(2));
%         dudx = exact_sol3_dx(coordy(1), coordy(2)); dudy = exact_sol3_dy(coordy(1), coordy(2));
%         dudxdx = exact_sol3_dxdx(coordy(1), coordy(2)); dudxdy = exact_sol3_dxdy(coordy(1), coordy(2)); 
%         dudydy = exact_sol3_dydy(coordy(1), coordy(2));
%         A0 = A0 + scafac * (ux-uy)^2 - scafac * s * ( dudx * v(1) + dudy * v(2) )^2 - ...
%             2 * scafac * s * (dudx * v(1) + dudy * v(2)) * ( 1/2 * dudxdx * v(1)^2 + dudxdy * v(1) * v(2) + 1/2 * dudydy * v(2)^2);
    end
    
end
% A = 2^(2*alpha) * gamma(1+alpha)/(pi*abs(gamma(-alpha))) * A / 2;
% A0 = 2^(2*alpha) * alpha * gamma(1+alpha)/(pi*gamma(1-alpha)) * A0 / 2;
% 
% 
% A0
% rhs0

bd = dirichletbd(nrb);
nrb = nrb.fill(0);
nrb.cval(~bd) = A(~bd,~bd)\rhs(~bd);

%%
plot(nrb)

