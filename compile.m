cd igafem-nurbs
!rm *.mex*
% mex NURBSbasis.c NURBS.c -outdir bin
% mex NURBSinterpolation.c NURBS.c -outdir bin
% mex NURBSinterpolation2d.c NURBS.c -outdir bin
% mex NURBS1DBasisDers.c NURBS.c -outdir bin
% mex NURBS1DBasis2ndDers.c NURBS.c -outdir bin
mex NURBS2DBasis.c NURBS.c -outdir bin
mex NURBS2DBasisDers.c NURBS.c -outdir bin
mex NURBS2DBasis2ndDers.c NURBS.c -outdir bin
mex NURBS2DBasisIndex.c NURBS.c -outdir bin
mex NURBS2DBasis3rdDers.c NURBS.c -outdir bin

% mex BSPLINE2DBasisDers.c NURBS.c -outdir bin
% mex NURBS2DBasisDersSpecial.c NURBS.c -outdir bin
% mex NURBSfdfd2fInterpolation.c NURBS.c -outdir bin
% mex NURBS3DBasisDers.c NURBS.c -outdir bin
% mex NURBS3DBasisDersSpecial.c NURBS.c -outdir bin
cd ..